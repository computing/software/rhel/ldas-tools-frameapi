# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define tarbasename ldas-tools-frameAPI
%define _docdir     %{_datadir}/doc/%{tarbasename}-%{version}

%define check_cmake3 ( 0%{?rhel} && 0%{?rhel} <= 7 )


Summary: LDAS tools libframeAPI toolkit runtime files
Name: ldas-tools-frameAPI
Version: 2.6.6
Release: 1.1%{?dist}
License: GPLv2+
URL: "https://wiki.ligo.org/Computing/LDASTools"
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: https://software.igwn.org/lscsoft/source/ldas-tools-frameAPI-%{version}.tar.gz
Requires: ldas-tools-framecpp >= 2.8.1
Requires: ldas-tools-filters >= 2.6.6
Requires: ldas-tools-ldasgen >= 2.7.3
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
%if %{check_cmake3}
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
Buildrequires: doxygen
Buildrequires: libtool
Buildrequires: openssl-devel
Buildrequires: pkgconfig
Buildrequires: zlib-devel
Buildrequires: ldas-tools-cmake >= 1.2.3
Buildrequires: ldas-tools-al-devel >= 2.6.7
Buildrequires: ldas-tools-framecpp-devel >= 2.8.1
Buildrequires: ldas-tools-filters-devel >= 2.6.6
Buildrequires: ldas-tools-ldasgen-devel >= 2.7.3

%description
This provides the runtime libraries for the frameAPI library.

%package devel
Group: Development/Scientific
Summary: LDAS tools libframeAPI toolkit development files
Requires: ldas-tools-al-devel >= 2.6.7
Requires: ldas-tools-filters-devel >= 2.6.6
Requires: ldas-tools-ldasgen-devel >= 2.7.3
Requires: ldas-tools-frameAPI = %{version}
%description devel
This provides the develpement files the frameAPI library.

%prep

%setup -c -T -D -a 0 -n %{name}-%{version}

%build
%if %{check_cmake3}
export CMAKE_PROGRAM=cmake3
%else
export CMAKE_PROGRAM=cmake
%endif

${CMAKE_PROGRAM} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    %{tarbasename}-%{version}

make VERBOSE=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;

%post
ldconfig

%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libldasframe*.so*

%files devel
%defattr(-,root,root)
%{_includedir}/frameAPI
%{_libdir}/libldasframe*.*a
%{_docdir}
%{_libdir}/pkgconfig/ldastools-ldasframe.pc

%changelog
* Wed Sep 29 2021 Edward Maros <ed.maros@ligo.org> - 2.6.6-1
- Built for new release

* Tue Mar 9 2021 Edward Maros <ed.maros@ligo.org> - 2.6.5-1
- Built for new release

* Wed Aug 14 2019 Edward Maros <ed.maros@ligo.org> - 2.6.4-1
- Built for new release

* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.2-1
- Built for new release

* Fri Sep 09 2016 Edward Maros <ed.maros@ligo.org> - 2.5.1-1
- Built for new release

* Thu Apr 07 2016 Edward Maros <ed.maros@ligo.org> - 2.5.0-1
- Built for new release

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
